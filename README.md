# CommandBox Git protect

As of CommandBox 5.1.0, `box install` in a project will overwrite any package (e.g. a Preside extension) where the current version does not match the installed version number - including downgrades.

This means that if your project has an extension which is a Git repo, which may likely have a placeholder such as `VERSION_NUMBER` which gets populated by a build script, it will _always_ get overwritten, which is not ideal.

This module adds protection against this: if the existing package is a Git repo, it will _never_ be overwritten.

## Requirements

CommandBox 5.2.0 or later is required for this to work, which modifies the `onInstall` interceptor to allow it to skip installation of a module.